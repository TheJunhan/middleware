package com.ist.middleware.controller;

import com.ist.middleware.util.Clients;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

@RestController
@RequestMapping("/notification")
public class NoticeController {
    @RequestMapping("/dataCenter")
    public void dataCenter(@Param("id") String id, @Param("todoNumber") Integer todoNumber) throws IOException {
        Socket socket = Clients.clients.get(id);
        OutputStream out = socket.getOutputStream();
        out.write(("dataCenter" + todoNumber).getBytes());
        out.flush();
    }

    @RequestMapping("/projectManagement")
    public void projectManagement(@Param("id") String id, @Param("todoNumber") Integer todoNumber) throws IOException {
        Socket socket = Clients.clients.get(id);
        OutputStream out = socket.getOutputStream();
        out.write(("projectManagement" + todoNumber).getBytes());
        out.flush();
    }
}
