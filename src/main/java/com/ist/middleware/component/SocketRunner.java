package com.ist.middleware.component;

import com.ist.middleware.util.Clients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

@Component
public class SocketRunner implements CommandLineRunner {
    @Autowired
    SocketProperties properties;

    @Override
    public void run(String... args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(properties.getPort());
        Socket socket = null;
        System.out.println("服务器端开始监听" + properties.getPort());
        while(true) {
            socket = serverSocket.accept();
            System.out.println("建立连接" + socket.toString());
            InputStream inputStream = socket.getInputStream();
            byte[] content = new byte[1024];
            int len = inputStream.read(content);
            StringBuilder id = new StringBuilder();
            for(int i = 0; i < len; ++i) {
                id.append((char)content[i]);
            }
            System.out.println(id);
            Clients.clients.put(id.toString(), socket);
        }
    }
}
